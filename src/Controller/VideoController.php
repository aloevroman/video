<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Video;
use App\Form\VideoType;
use App\Services\Video\Interfaces\VideoServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VideoController extends AbstractController
{
    /**
     * @Route("/show_videos", name="show_videos")
     */
    public function showVideos(VideoServiceInterface $videoService): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('video/show_videos.html.twig', [
            'videos' => $videoService->getAllVideos()
        ]);
    }

    /**
     * @Route("/show_video/{youtube_id}", name="show_video")
     */
    public function showVideo(string $youtube_id): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $video = $this->getDoctrine()
            ->getRepository(Video::class)
            ->findBy(['youtube_id' => $youtube_id]);

        return $this->render('video/show_video.html.twig', [
            'video' => $video[0]
        ]);
    }

    /**
     * @Route("/add_video", name="add_video")
     */
    public function addVideo(Request $request): Response
    {
        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($video);
            $entityManager->flush();

            return $this->redirectToRoute('show_videos', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('video/add_video.html.twig', [
            'video' => $video,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/show_video_by_cat/{category_id}", name="show_video_by_cat")
     */
    public function showByCategory(string $category_id, VideoServiceInterface $videoService): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('video/show_videos.html.twig', [
            'videos' => $videoService->getVideosByCategory($category_id)
        ]);
    }

    /**
     * @Route("/{id}", name="video_delete", methods={"POST"})
     */
    public function delete(Request $request, Video $video): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($video);
        $entityManager->flush();

        return $this->redirectToRoute('show_videos', [], Response::HTTP_SEE_OTHER);
    }
}
