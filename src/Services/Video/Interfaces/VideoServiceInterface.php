<?php

declare(strict_types=1);

namespace App\Services\Video\Interfaces;

/**
 * Video service interface for manage video
 */
interface VideoServiceInterface
{
    /**
     * Must return full list of videos
     * @return mixed
     */
    public function getAllVideos(): array;

    /**
     * Must return all videos by category
     * @return mixed
     */
    public function getVideosByCategory(string $categoryId): array;
}