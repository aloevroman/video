<?php

declare(strict_types=1);

namespace App\Services\Video;

use App\Repository\Video\Interfaces\VideoRepositoryInterface;
use App\Services\Video\Interfaces\VideoServiceInterface;

class VideoService implements VideoServiceInterface
{
    private $repository;

    public function __construct(VideoRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getAllVideos(): array
    {
        return $this->repository->findAll();
    }

    public function getVideosByCategory(string $categoryId): array
    {
        return $this->repository->findByCategory($categoryId);
    }
}