<?php

declare(strict_types=1);

namespace App\Repository\Video\Interfaces;

/**
 * Video repository interface for video storage manipulations
 */
interface VideoRepositoryInterface
{
    /**
     * Find all videos by category
     * @param string $category_id
     */
    public function findByCategory(string $category_id);
}