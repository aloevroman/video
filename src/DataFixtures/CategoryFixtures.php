<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Seed categories
 */
class CategoryFixtures extends Fixture
{
    /**
     * Names of categories
     * @var string[]
     */
    private $categoryNames = [
        'PHP',
        'JS',
        'Java',
        'C++',
    ];

    public function load(ObjectManager $manager)
    {
        foreach ($this->categoryNames as $categoryName) {
            $video = new Category();
            $video->setTitle($categoryName);
            $manager->persist($video);
        }

        $manager->flush();
    }
}
