<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Video;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Seed videos
 */
class VideoFixtures extends Fixture
{
    /**
     * Time codes from youtube videos
     * @var array
     */
    private $youtubeTimeCodes = [
        'e53eao95ZyM',
        '-vn5rSWrJG8',
        'DG2DfnXTaRM',
        'ggq5tSlmQvg',
        'oNZ9dIHOZLU',
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->youtubeTimeCodes as $youtubeTimeCode) {
            $video = new Video();
            $video->setTitle('title_' . $youtubeTimeCode);
            $video->setCategoryId(mt_rand(1, 4));
            $video->setYoutubeId($youtubeTimeCode);
            $manager->persist($video);
        }

        $manager->flush();
    }
}
